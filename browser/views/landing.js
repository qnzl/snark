const html = require('choo/html')

module.exports = (state, emit) => {
  return html`
    <div>
      <div class="flex flex-column items-center">
        <h3>Shouldn't you be doing something better?</h3>

        <div class="pb4">
          You blocked the site on purpose. 
          I take it since you are going to a wasteful place, you've accomplished all of this:
        </div>
        <div class="pb4">
          <div>
            <input type="checkbox" name="cleaned"> Cleaned your home
          </div>
          <div>
            <input type="checkbox" name="washed"> Washed your dishes
          </div>
          <div>
            <input type="checkbox" name="eaten"> Eaten, if you are hungry
          </div>
          <div>
            <input type="checkbox" name="workFinish"> Finished your work
          </div>
          <div>
            <input type="checkbox" name="read"> Read a book
          </div>
          <div>
            <input type="checkbox" name="meditate"> Meditated
          </div>
          <div>
            <input type="checkbox" name="exercise"> Exercised
          </div>
          <div>
            <input type="checkbox" name="social"> Socialized
          </div>
          <div>
            <input type="checkbox" name="journal"> Journaled
          </div>
          <div>
            <input type="checkbox" name="bath"> Taken a relaxing bath or shower
          </div>
          <div>
            <input type="checkbox" name="walk"> Taken a walk
          </div>
          <div>
            <input type="checkbox" name="walk"> Sleep, if you're tired
          </div>
        </div>
      </div>
      <div class="fixed-ns relative flex justify-center f7 pa2-ns pa2" style="right:0;bottom:0">
        Created by <a rel="noreferrer" href="https://twitter.com/madamelic" class="black pl1" target="_blank">@madamelic</a>
      </div>
    </div>
  `
}